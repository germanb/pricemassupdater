<?php

namespace Germanb\PriceMassUpdater\Model;

use \Magento\Framework\ObjectManagerInterface;

use Germanb\PriceMassUpdater\Api\PriceMassUpdaterInterface as ApiInterface;


class PriceMassUpdater implements ApiInterface {
    protected $objectManager;

    /**
     * Constructor.
     */
    public function __construct(ObjectManagerInterface $manager) {
        $this->objectManager = $manager;
    }

    /**
     * Gets the object manager.
     * 
     * @return ObjectManager $objectManager
     */
    protected function getObjectManager() {
        return $this->objectManager;
    }

    /**
     * Updates all Products.
     * 
     * @param mixed $products
     * @return array
     */
    public function updatePrices($products) {
        try {
            // Updates all prices
            foreach ($products as $product) {
                // Get product data.
                $entity_id = $producto[1];
                $price = $producto[2];
                $last_price = $producto[3];

                // I go through with the update only if I have a valid price.
                if ($price > 0) {
                    // Check if I have a special price.
                    if ($last_price != 0) {
                        // The special price MUST be lower than the current one.
                        if ($last_price > $price) {
                            $current_price = $last_price;
                            $special_price = $price;
                        } else {
                            $current_price = $price;
                            $special_price = 0;
                        }
                    } else {
                        $current_price = $price;
                        $special_price = 0;
                    }

                    // Update!
                    $this->updateBasePrice($entity_id, $current_price);
                    $this->updateSpecialPrice($entity_id, $special_price);
                }
            }

            return json_encode(
                [
                    'State' => 200, 
                    'Message' => 'I updated '.count($productos).' products'
                ]
            );
        } catch (\Exception $e) {
            return json_encode(
                [
                    'State' => 500, 
                    'Message' => $e->getMessage()
                ]
            );
        }
    }

    /**
     * Update the base price of a product.
     * 
     * @param integer $entity_id
     * @param float $price
     * @return boolean $updated
     */
    private function updateBasePrice($entity_id, $price) {
        try {
            // Get the product.
            $manager = $this->getObjectManager();
            $product = $manager->create('\Magento\Catalog\Model\ProductFactory');
            $productResourceModel = $manager->create('\Magento\Catalog\Model\ResourceModel\Product');
            $productFactory = $product->create();
            $productResourceModel->load($productFactory, $entity_id);
            $productFactory->setStoreId(0);
            // Update!
            $productFactory->setPrice($price);
            $productResourceModel->saveAttribute($productFactory, 'price');

            return true;
        } catch (\Exception $e) {
            throw \Exception($e);
        }
    }

    /**
     * Update the special price of a product.
     * 
     * @param integer $entity_id
     * @param float $special_price
     * @return boolean $updated
     */
    private function updateSpecialPrice($entity_id, $special_price) {
        try {
            // Get the product.
            $manager = $this->getObjectManager();
            $product = $manager->create('\Magento\Catalog\Model\ProductFactory');
            $productResourceModel = $manager->create('\Magento\Catalog\Model\ResourceModel\Product');
            $productFactory = $product->create();
            $productResourceModel->load($productFactory, $entity_id);
            $productFactory->setStoreId(0);
            // Get from-to dates (One year in this case).
            if ($special_price > 0) {
                $from = date('Y-m-d', time());
                $to = date('Y-m-d', time() + (60*60*24*7*52));
            } else {
                $special_price = 0;
                $from = null;
                $to = null;
            }
            // Set the special price.
            $productFactory->setSpecialPrice($special_price);
            $productFactory->setSpecialFromDate($from);
            $productFactory->setSpecialToDate($to);
            
            // Save everything.
            $productResourceModel->saveAttribute($productFactory, 'special_price');
            $productResourceModel->saveAttribute($productFactory, 'special_from_date');
            $productResourceModel->saveAttribute($productFactory, 'special_to_date');

            return true;
        } catch (\Exception $e) {
            throw new \Exception($e);
        }
    }
}
