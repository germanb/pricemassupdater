<?php
namespace Germanb\PriceMassUpdater\Api;
 
interface PriceMassUpdaterInterface {
    /**
     * Calls for the price update.
     * 
     * @param mixed $products
     * @return string $result
     */
    public function updateProducts($products);
}
